(function(){
   'use strict';
   
    angular.module('linked',['ngResource','linked.controllers','linked.services'])
    
    .config(['$resourceProvider',function($resourceProvider){
       $resourceProvider.defaults.stripTrailingSlashes = false;
    }])
    
})();
(function(){
   'use strict';
   angular.module('linked.controllers',[])
   
   .controller('DBpediaController',['$scope','DBpediaService',function($scope,DBpediaService){
      var query = "PREFIX type: <http://dbpedia.org/class/yago/>\
                  PREFIX prop: <http://dbpedia.org/property/>\
                  SELECT ?country_name ?population\
                  WHERE {\
                        ?country a type:LandlockedCountries ;\
                        rdfs:label ?country_name ;\
                        prop:populationEstimate ?population .\
                        FILTER (?population > 15000000 &&\
                        langMatches(lang(?country_name), 'EN')) .\
                  } ORDER BY DESC(?population)";
      DBpediaService.get({query:query,format:'json'},function(data){
         $scope.respuesta=data;
         console.log(data);
      })
   }]);
   
})();